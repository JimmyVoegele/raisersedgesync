﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace raisersedgesync
{
    public partial class _Default : Page
    {
        private static string m_strToken = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                string strCode = Request.Params["code"];
                if (!string.IsNullOrEmpty(strCode) && string.IsNullOrEmpty(m_strToken))
                {
                    m_strToken = GetToken(strCode);
                }
            }
        }

        protected void btnAuthorize_Click(object sender, EventArgs e)
        {
            m_strToken = "";
            string strURL = ConfigurationManager.AppSettings["AuthorizationURL"].ToString();
            string strResponseType = "response_type=code";
            string strClientId = "client_id=" + ConfigurationManager.AppSettings["ClientID"].ToString(); ;
            string strRedirectURI = "redirect_uri=" + ConfigurationManager.AppSettings["RedirectURI"].ToString(); ;
            //string strResponse = "https://oauth2.sky.blackbaud.com/authorization?response_type=code&client_id=d908593f-68ff-45a7-a66e-9e936bb4f599&redirect_uri=http://localhost:50379/Default";
            string strResponsenew = strURL + strResponseType + "&" + strClientId + "&" + strRedirectURI;
            Response.Redirect(strResponsenew);
        }

        private string GetToken(string strCode)
        {

            string strToken = "";

            try
            {
                //            var request = (HttpWebRequest)WebRequest.Create("https://oauth2.sky.blackbaud.com/token?code=1c3d7dc147b6467e86f235f550da9ce8&grant_type=authorization_code&redirect_uri=http://localhost:50379/Default");
                var request = (HttpWebRequest)WebRequest.Create("https://oauth2.sky.blackbaud.com/token");

                var postData = "code=" + strCode;
                postData += "&grant_type=authorization_code";
                postData += "&redirect_uri=" + ConfigurationManager.AppSettings["RedirectURI"].ToString();

                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "Post";
                request.Host = ConfigurationManager.AppSettings["Host"].ToString();
                request.ContentType = "application/x-www-form-urlencoded";

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                request.Headers.Add(HttpRequestHeader.Authorization, ConfigurationManager.AppSettings["TokenAuthorization"].ToString());

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                dynamic strJSON = JsonConvert.DeserializeObject(responseString);

                strToken = strJSON.access_token;

                if (!string.IsNullOrEmpty(strToken))
                {
                    lblStatus.Text = "Token Received Successfully";
                }
            }catch (Exception ex)
            {
                lblStatus.Text = "Error Getting Token:" + ex.Message;
                strToken = "";
            }

            return strToken;
        }



        private void GetAddresses(string strToken)
        {
            var request = (HttpWebRequest)WebRequest.Create("https://api.sky.blackbaud.com/constituent/v1/addresses");

            request.Host = "api.sky.blackbaud.com";

            request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + strToken);
            request.Headers.Add("bb-api-subscription-key", "50c88580606d4f4abfa0097eecc79d30");


            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();


            System.Xml.XmlDocument newArr = JsonConvert.DeserializeXmlNode(responseString, "value");

            //dynamic strJSON = JsonConvert.DeserializeObject(responseString);

            //string strAddresses = strJSON.access_token;
        }
    }
}