﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="raisersedgesync._Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1> Raisers Edge Synchronization application</h1>
    <telerik:RadLabel ID="RadLabel2" runat="server" Text ="Please click the Authorize button to begin:"></telerik:RadLabel>
    <br />  
    <br />  
    <asp:Button ID="btnAuthorize" runat="server" Text="Authorize" OnClick="btnAuthorize_Click" />
    <br />
    <br />
    <telerik:RadLabel ID="RadLabel1" runat="server" Text ="Status:"></telerik:RadLabel>
    <telerik:RadLabel ID="lblStatus" runat="server" Text="Waiting to authenticate"></telerik:RadLabel>
</asp:Content>
